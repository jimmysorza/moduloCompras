/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "regimenes_dian")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegimenesDian.findAll", query = "SELECT r FROM RegimenesDian r")
    , @NamedQuery(name = "RegimenesDian.findByIdRegimenesDian", query = "SELECT r FROM RegimenesDian r WHERE r.idRegimenesDian = :idRegimenesDian")
    , @NamedQuery(name = "RegimenesDian.findByNombre", query = "SELECT r FROM RegimenesDian r WHERE r.nombre = :nombre")
    , @NamedQuery(name = "RegimenesDian.findByUltimaActualizacion", query = "SELECT r FROM RegimenesDian r WHERE r.ultimaActualizacion = :ultimaActualizacion")})
public class RegimenesDian implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Regimenes_Dian")
    private Integer idRegimenesDian;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ultima_Actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaActualizacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRegimenDian")
    private Collection<RazonesSociales> razonesSocialesCollection;

    public RegimenesDian() {
    }

    public RegimenesDian(Integer idRegimenesDian) {
        this.idRegimenesDian = idRegimenesDian;
    }

    public RegimenesDian(Integer idRegimenesDian, String nombre, Date ultimaActualizacion) {
        this.idRegimenesDian = idRegimenesDian;
        this.nombre = nombre;
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public Integer getIdRegimenesDian() {
        return idRegimenesDian;
    }

    public void setIdRegimenesDian(Integer idRegimenesDian) {
        this.idRegimenesDian = idRegimenesDian;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    @XmlTransient
    public Collection<RazonesSociales> getRazonesSocialesCollection() {
        return razonesSocialesCollection;
    }

    public void setRazonesSocialesCollection(Collection<RazonesSociales> razonesSocialesCollection) {
        this.razonesSocialesCollection = razonesSocialesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegimenesDian != null ? idRegimenesDian.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegimenesDian)) {
            return false;
        }
        RegimenesDian other = (RegimenesDian) object;
        if ((this.idRegimenesDian == null && other.idRegimenesDian != null) || (this.idRegimenesDian != null && !this.idRegimenesDian.equals(other.idRegimenesDian))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.RegimenesDian[ idRegimenesDian=" + idRegimenesDian + " ]";
    }
    
}
