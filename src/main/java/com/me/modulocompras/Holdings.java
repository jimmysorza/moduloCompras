/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "holdings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Holdings.findAll", query = "SELECT h FROM Holdings h")
    , @NamedQuery(name = "Holdings.findByIdHolding", query = "SELECT h FROM Holdings h WHERE h.idHolding = :idHolding")
    , @NamedQuery(name = "Holdings.findByNombre", query = "SELECT h FROM Holdings h WHERE h.nombre = :nombre")
    , @NamedQuery(name = "Holdings.findByUltimaActualizacion", query = "SELECT h FROM Holdings h WHERE h.ultimaActualizacion = :ultimaActualizacion")})
public class Holdings implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Holding")
    private Integer idHolding;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ultima_Actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaActualizacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idHolding")
    private Collection<Empresas> empresasCollection;

    public Holdings() {
    }

    public Holdings(Integer idHolding) {
        this.idHolding = idHolding;
    }

    public Holdings(Integer idHolding, String nombre, Date ultimaActualizacion) {
        this.idHolding = idHolding;
        this.nombre = nombre;
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public Integer getIdHolding() {
        return idHolding;
    }

    public void setIdHolding(Integer idHolding) {
        this.idHolding = idHolding;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    @XmlTransient
    public Collection<Empresas> getEmpresasCollection() {
        return empresasCollection;
    }

    public void setEmpresasCollection(Collection<Empresas> empresasCollection) {
        this.empresasCollection = empresasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHolding != null ? idHolding.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Holdings)) {
            return false;
        }
        Holdings other = (Holdings) object;
        if ((this.idHolding == null && other.idHolding != null) || (this.idHolding != null && !this.idHolding.equals(other.idHolding))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.Holdings[ idHolding=" + idHolding + " ]";
    }
    
}
