/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "razones_sociales")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RazonesSociales.findAll", query = "SELECT r FROM RazonesSociales r")
    , @NamedQuery(name = "RazonesSociales.findByIdRazonSocial", query = "SELECT r FROM RazonesSociales r WHERE r.idRazonSocial = :idRazonSocial")
    , @NamedQuery(name = "RazonesSociales.findByNit", query = "SELECT r FROM RazonesSociales r WHERE r.nit = :nit")
    , @NamedQuery(name = "RazonesSociales.findByTipoContribuyente", query = "SELECT r FROM RazonesSociales r WHERE r.tipoContribuyente = :tipoContribuyente")
    , @NamedQuery(name = "RazonesSociales.findByRazonSocial", query = "SELECT r FROM RazonesSociales r WHERE r.razonSocial = :razonSocial")
    , @NamedQuery(name = "RazonesSociales.findByNombreComercial", query = "SELECT r FROM RazonesSociales r WHERE r.nombreComercial = :nombreComercial")
    , @NamedQuery(name = "RazonesSociales.findByRutImagen", query = "SELECT r FROM RazonesSociales r WHERE r.rutImagen = :rutImagen")
    , @NamedQuery(name = "RazonesSociales.findByDireccion", query = "SELECT r FROM RazonesSociales r WHERE r.direccion = :direccion")
    , @NamedQuery(name = "RazonesSociales.findByTelefono", query = "SELECT r FROM RazonesSociales r WHERE r.telefono = :telefono")
    , @NamedQuery(name = "RazonesSociales.findByUltimaActualizacion", query = "SELECT r FROM RazonesSociales r WHERE r.ultimaActualizacion = :ultimaActualizacion")})
public class RazonesSociales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Razon_Social")
    private Integer idRazonSocial;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Nit")
    private String nit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Tipo_Contribuyente")
    private String tipoContribuyente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Razon_Social")
    private String razonSocial;
    @Size(max = 45)
    @Column(name = "Nombre_Comercial")
    private String nombreComercial;
    @Size(max = 45)
    @Column(name = "Rut_Imagen")
    private String rutImagen;
    @Size(max = 45)
    @Column(name = "Direccion")
    private String direccion;
    @Size(max = 45)
    @Column(name = "Telefono")
    private String telefono;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ultima_Actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaActualizacion;
    @JoinColumn(name = "IdTipo_Contribuyente", referencedColumnName = "idTipo_Contribuyente")
    @ManyToOne(optional = false)
    private TipoContribuyente idTipoContribuyente;
    @JoinColumn(name = "Id_Regimen_Dian", referencedColumnName = "Id_Regimenes_Dian")
    @ManyToOne(optional = false)
    private RegimenesDian idRegimenDian;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRazonSocial")
    private Collection<Sucursales> sucursalesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRazonSocial")
    private Collection<Empresas> empresasCollection;

    public RazonesSociales() {
    }

    public RazonesSociales(Integer idRazonSocial) {
        this.idRazonSocial = idRazonSocial;
    }

    public RazonesSociales(Integer idRazonSocial, String nit, String tipoContribuyente, String razonSocial, Date ultimaActualizacion) {
        this.idRazonSocial = idRazonSocial;
        this.nit = nit;
        this.tipoContribuyente = tipoContribuyente;
        this.razonSocial = razonSocial;
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public Integer getIdRazonSocial() {
        return idRazonSocial;
    }

    public void setIdRazonSocial(Integer idRazonSocial) {
        this.idRazonSocial = idRazonSocial;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getTipoContribuyente() {
        return tipoContribuyente;
    }

    public void setTipoContribuyente(String tipoContribuyente) {
        this.tipoContribuyente = tipoContribuyente;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getNombreComercial() {
        return nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getRutImagen() {
        return rutImagen;
    }

    public void setRutImagen(String rutImagen) {
        this.rutImagen = rutImagen;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public TipoContribuyente getIdTipoContribuyente() {
        return idTipoContribuyente;
    }

    public void setIdTipoContribuyente(TipoContribuyente idTipoContribuyente) {
        this.idTipoContribuyente = idTipoContribuyente;
    }

    public RegimenesDian getIdRegimenDian() {
        return idRegimenDian;
    }

    public void setIdRegimenDian(RegimenesDian idRegimenDian) {
        this.idRegimenDian = idRegimenDian;
    }

    @XmlTransient
    public Collection<Sucursales> getSucursalesCollection() {
        return sucursalesCollection;
    }

    public void setSucursalesCollection(Collection<Sucursales> sucursalesCollection) {
        this.sucursalesCollection = sucursalesCollection;
    }

    @XmlTransient
    public Collection<Empresas> getEmpresasCollection() {
        return empresasCollection;
    }

    public void setEmpresasCollection(Collection<Empresas> empresasCollection) {
        this.empresasCollection = empresasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRazonSocial != null ? idRazonSocial.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RazonesSociales)) {
            return false;
        }
        RazonesSociales other = (RazonesSociales) object;
        if ((this.idRazonSocial == null && other.idRazonSocial != null) || (this.idRazonSocial != null && !this.idRazonSocial.equals(other.idRazonSocial))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.RazonesSociales[ idRazonSocial=" + idRazonSocial + " ]";
    }
    
}
