/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "pagos_compras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PagosCompras.findAll", query = "SELECT p FROM PagosCompras p")
    , @NamedQuery(name = "PagosCompras.findByIdPagoCompra", query = "SELECT p FROM PagosCompras p WHERE p.idPagoCompra = :idPagoCompra")
    , @NamedQuery(name = "PagosCompras.findByFechaPago", query = "SELECT p FROM PagosCompras p WHERE p.fechaPago = :fechaPago")
    , @NamedQuery(name = "PagosCompras.findByValor", query = "SELECT p FROM PagosCompras p WHERE p.valor = :valor")})
public class PagosCompras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Pago_Compra")
    private Integer idPagoCompra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha_Pago")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPago;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Valor")
    private String valor;
    @JoinColumn(name = "Id_Factura_Compra", referencedColumnName = "Id_Factura_Compra")
    @ManyToOne(optional = false)
    private FacturasCompras idFacturaCompra;

    public PagosCompras() {
    }

    public PagosCompras(Integer idPagoCompra) {
        this.idPagoCompra = idPagoCompra;
    }

    public PagosCompras(Integer idPagoCompra, Date fechaPago, String valor) {
        this.idPagoCompra = idPagoCompra;
        this.fechaPago = fechaPago;
        this.valor = valor;
    }

    public Integer getIdPagoCompra() {
        return idPagoCompra;
    }

    public void setIdPagoCompra(Integer idPagoCompra) {
        this.idPagoCompra = idPagoCompra;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public FacturasCompras getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(FacturasCompras idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPagoCompra != null ? idPagoCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagosCompras)) {
            return false;
        }
        PagosCompras other = (PagosCompras) object;
        if ((this.idPagoCompra == null && other.idPagoCompra != null) || (this.idPagoCompra != null && !this.idPagoCompra.equals(other.idPagoCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.PagosCompras[ idPagoCompra=" + idPagoCompra + " ]";
    }
    
}
