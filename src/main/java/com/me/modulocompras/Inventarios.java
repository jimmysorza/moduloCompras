/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "inventarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventarios.findAll", query = "SELECT i FROM Inventarios i")
    , @NamedQuery(name = "Inventarios.findByIdInventario", query = "SELECT i FROM Inventarios i WHERE i.idInventario = :idInventario")
    , @NamedQuery(name = "Inventarios.findByExistencia", query = "SELECT i FROM Inventarios i WHERE i.existencia = :existencia")})
public class Inventarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Inventario")
    private Integer idInventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Existencia")
    private int existencia;
    @JoinColumn(name = "Id_Producto", referencedColumnName = "Id_Producto")
    @ManyToOne(optional = false)
    private Productos idProducto;
    @JoinColumn(name = "Id_Sucursal", referencedColumnName = "Id_Sucursal")
    @ManyToOne(optional = false)
    private Sucursales idSucursal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInventario")
    private Collection<DetalleFacturaCompras> detalleFacturaComprasCollection;

    public Inventarios() {
    }

    public Inventarios(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public Inventarios(Integer idInventario, int existencia) {
        this.idInventario = idInventario;
        this.existencia = existencia;
    }

    public Integer getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Integer idInventario) {
        this.idInventario = idInventario;
    }

    public int getExistencia() {
        return existencia;
    }

    public void setExistencia(int existencia) {
        this.existencia = existencia;
    }

    public Productos getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Productos idProducto) {
        this.idProducto = idProducto;
    }

    public Sucursales getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Sucursales idSucursal) {
        this.idSucursal = idSucursal;
    }

    @XmlTransient
    public Collection<DetalleFacturaCompras> getDetalleFacturaComprasCollection() {
        return detalleFacturaComprasCollection;
    }

    public void setDetalleFacturaComprasCollection(Collection<DetalleFacturaCompras> detalleFacturaComprasCollection) {
        this.detalleFacturaComprasCollection = detalleFacturaComprasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInventario != null ? idInventario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventarios)) {
            return false;
        }
        Inventarios other = (Inventarios) object;
        if ((this.idInventario == null && other.idInventario != null) || (this.idInventario != null && !this.idInventario.equals(other.idInventario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.Inventarios[ idInventario=" + idInventario + " ]";
    }
    
}
