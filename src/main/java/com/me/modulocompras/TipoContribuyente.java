/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "Tipo_Contribuyente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoContribuyente.findAll", query = "SELECT t FROM TipoContribuyente t")
    , @NamedQuery(name = "TipoContribuyente.findByIdTipoContribuyente", query = "SELECT t FROM TipoContribuyente t WHERE t.idTipoContribuyente = :idTipoContribuyente")
    , @NamedQuery(name = "TipoContribuyente.findByNombre", query = "SELECT t FROM TipoContribuyente t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TipoContribuyente.findByUltimaActualizacion", query = "SELECT t FROM TipoContribuyente t WHERE t.ultimaActualizacion = :ultimaActualizacion")})
public class TipoContribuyente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTipo_Contribuyente")
    private Integer idTipoContribuyente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Ultima_Actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimaActualizacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoContribuyente")
    private Collection<RazonesSociales> razonesSocialesCollection;

    public TipoContribuyente() {
    }

    public TipoContribuyente(Integer idTipoContribuyente) {
        this.idTipoContribuyente = idTipoContribuyente;
    }

    public TipoContribuyente(Integer idTipoContribuyente, String nombre, Date ultimaActualizacion) {
        this.idTipoContribuyente = idTipoContribuyente;
        this.nombre = nombre;
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public Integer getIdTipoContribuyente() {
        return idTipoContribuyente;
    }

    public void setIdTipoContribuyente(Integer idTipoContribuyente) {
        this.idTipoContribuyente = idTipoContribuyente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    @XmlTransient
    public Collection<RazonesSociales> getRazonesSocialesCollection() {
        return razonesSocialesCollection;
    }

    public void setRazonesSocialesCollection(Collection<RazonesSociales> razonesSocialesCollection) {
        this.razonesSocialesCollection = razonesSocialesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoContribuyente != null ? idTipoContribuyente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoContribuyente)) {
            return false;
        }
        TipoContribuyente other = (TipoContribuyente) object;
        if ((this.idTipoContribuyente == null && other.idTipoContribuyente != null) || (this.idTipoContribuyente != null && !this.idTipoContribuyente.equals(other.idTipoContribuyente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.TipoContribuyente[ idTipoContribuyente=" + idTipoContribuyente + " ]";
    }
    
}
