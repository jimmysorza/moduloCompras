/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author jimmysorza
 */
@Stateless
public class HoldingsFacade extends AbstractFacade<Holdings> {

    @PersistenceContext(unitName = "com.me_moduloCompras_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HoldingsFacade() {
        super(Holdings.class);
    }
    
}
