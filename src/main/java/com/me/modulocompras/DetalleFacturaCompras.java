/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "detalle_factura_compras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleFacturaCompras.findAll", query = "SELECT d FROM DetalleFacturaCompras d")
    , @NamedQuery(name = "DetalleFacturaCompras.findByIdDetalleFacturaCompra", query = "SELECT d FROM DetalleFacturaCompras d WHERE d.idDetalleFacturaCompra = :idDetalleFacturaCompra")
    , @NamedQuery(name = "DetalleFacturaCompras.findByCantidad", query = "SELECT d FROM DetalleFacturaCompras d WHERE d.cantidad = :cantidad")
    , @NamedQuery(name = "DetalleFacturaCompras.findByDescuento", query = "SELECT d FROM DetalleFacturaCompras d WHERE d.descuento = :descuento")
    , @NamedQuery(name = "DetalleFacturaCompras.findByTotal", query = "SELECT d FROM DetalleFacturaCompras d WHERE d.total = :total")})
public class DetalleFacturaCompras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Detalle_Factura_Compra")
    private Integer idDetalleFacturaCompra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cantidad")
    private int cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Descuento")
    private double descuento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Total")
    private double total;
    @JoinColumn(name = "Id_Factura_Compra", referencedColumnName = "Id_Factura_Compra")
    @ManyToOne(optional = false)
    private FacturasCompras idFacturaCompra;
    @JoinColumn(name = "Id_Inventario", referencedColumnName = "Id_Inventario")
    @ManyToOne(optional = false)
    private Inventarios idInventario;

    public DetalleFacturaCompras() {
    }

    public DetalleFacturaCompras(Integer idDetalleFacturaCompra) {
        this.idDetalleFacturaCompra = idDetalleFacturaCompra;
    }

    public DetalleFacturaCompras(Integer idDetalleFacturaCompra, int cantidad, double descuento, double total) {
        this.idDetalleFacturaCompra = idDetalleFacturaCompra;
        this.cantidad = cantidad;
        this.descuento = descuento;
        this.total = total;
    }

    public Integer getIdDetalleFacturaCompra() {
        return idDetalleFacturaCompra;
    }

    public void setIdDetalleFacturaCompra(Integer idDetalleFacturaCompra) {
        this.idDetalleFacturaCompra = idDetalleFacturaCompra;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public FacturasCompras getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(FacturasCompras idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Inventarios getIdInventario() {
        return idInventario;
    }

    public void setIdInventario(Inventarios idInventario) {
        this.idInventario = idInventario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetalleFacturaCompra != null ? idDetalleFacturaCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleFacturaCompras)) {
            return false;
        }
        DetalleFacturaCompras other = (DetalleFacturaCompras) object;
        if ((this.idDetalleFacturaCompra == null && other.idDetalleFacturaCompra != null) || (this.idDetalleFacturaCompra != null && !this.idDetalleFacturaCompra.equals(other.idDetalleFacturaCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.DetalleFacturaCompras[ idDetalleFacturaCompra=" + idDetalleFacturaCompra + " ]";
    }
    
}
