/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "clase_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClaseProducto.findAll", query = "SELECT c FROM ClaseProducto c")
    , @NamedQuery(name = "ClaseProducto.findByIdClaseProducto", query = "SELECT c FROM ClaseProducto c WHERE c.idClaseProducto = :idClaseProducto")
    , @NamedQuery(name = "ClaseProducto.findByNombre", query = "SELECT c FROM ClaseProducto c WHERE c.nombre = :nombre")})
public class ClaseProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Clase_Producto")
    private Integer idClaseProducto;
    @Size(max = 45)
    @Column(name = "Nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idClaseProducto")
    private Collection<TipoProducto> tipoProductoCollection;

    public ClaseProducto() {
    }

    public ClaseProducto(Integer idClaseProducto) {
        this.idClaseProducto = idClaseProducto;
    }

    public Integer getIdClaseProducto() {
        return idClaseProducto;
    }

    public void setIdClaseProducto(Integer idClaseProducto) {
        this.idClaseProducto = idClaseProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<TipoProducto> getTipoProductoCollection() {
        return tipoProductoCollection;
    }

    public void setTipoProductoCollection(Collection<TipoProducto> tipoProductoCollection) {
        this.tipoProductoCollection = tipoProductoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClaseProducto != null ? idClaseProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaseProducto)) {
            return false;
        }
        ClaseProducto other = (ClaseProducto) object;
        if ((this.idClaseProducto == null && other.idClaseProducto != null) || (this.idClaseProducto != null && !this.idClaseProducto.equals(other.idClaseProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.ClaseProducto[ idClaseProducto=" + idClaseProducto + " ]";
    }
    
}
