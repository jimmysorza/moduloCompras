/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "formas_pago")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FormasPago.findAll", query = "SELECT f FROM FormasPago f")
    , @NamedQuery(name = "FormasPago.findByIdFormasPago", query = "SELECT f FROM FormasPago f WHERE f.idFormasPago = :idFormasPago")
    , @NamedQuery(name = "FormasPago.findByNombre", query = "SELECT f FROM FormasPago f WHERE f.nombre = :nombre")
    , @NamedQuery(name = "FormasPago.findByDias", query = "SELECT f FROM FormasPago f WHERE f.dias = :dias")})
public class FormasPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Formas_Pago")
    private Integer idFormasPago;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Dias")
    private int dias;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFormasPago")
    private Collection<FacturasCompras> facturasComprasCollection;

    public FormasPago() {
    }

    public FormasPago(Integer idFormasPago) {
        this.idFormasPago = idFormasPago;
    }

    public FormasPago(Integer idFormasPago, String nombre, int dias) {
        this.idFormasPago = idFormasPago;
        this.nombre = nombre;
        this.dias = dias;
    }

    public Integer getIdFormasPago() {
        return idFormasPago;
    }

    public void setIdFormasPago(Integer idFormasPago) {
        this.idFormasPago = idFormasPago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int dias) {
        this.dias = dias;
    }

    @XmlTransient
    public Collection<FacturasCompras> getFacturasComprasCollection() {
        return facturasComprasCollection;
    }

    public void setFacturasComprasCollection(Collection<FacturasCompras> facturasComprasCollection) {
        this.facturasComprasCollection = facturasComprasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFormasPago != null ? idFormasPago.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormasPago)) {
            return false;
        }
        FormasPago other = (FormasPago) object;
        if ((this.idFormasPago == null && other.idFormasPago != null) || (this.idFormasPago != null && !this.idFormasPago.equals(other.idFormasPago))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.FormasPago[ idFormasPago=" + idFormasPago + " ]";
    }
    
}
