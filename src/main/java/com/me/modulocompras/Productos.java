/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "productos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Productos.findAll", query = "SELECT p FROM Productos p")
    , @NamedQuery(name = "Productos.findByIdProducto", query = "SELECT p FROM Productos p WHERE p.idProducto = :idProducto")
    , @NamedQuery(name = "Productos.findByCodigoBarras", query = "SELECT p FROM Productos p WHERE p.codigoBarras = :codigoBarras")
    , @NamedQuery(name = "Productos.findByModeloReferencia", query = "SELECT p FROM Productos p WHERE p.modeloReferencia = :modeloReferencia")
    , @NamedQuery(name = "Productos.findByDescripcion", query = "SELECT p FROM Productos p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Productos.findByCosto", query = "SELECT p FROM Productos p WHERE p.costo = :costo")
    , @NamedQuery(name = "Productos.findByPrecio", query = "SELECT p FROM Productos p WHERE p.precio = :precio")
    , @NamedQuery(name = "Productos.findBySerial", query = "SELECT p FROM Productos p WHERE p.serial = :serial")
    , @NamedQuery(name = "Productos.findByImagen", query = "SELECT p FROM Productos p WHERE p.imagen = :imagen")})
public class Productos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Producto")
    private Integer idProducto;
    @Size(max = 45)
    @Column(name = "Codigo_Barras")
    private String codigoBarras;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "Modelo_Referencia")
    private String modeloReferencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Costo")
    private double costo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Precio")
    private double precio;
    @Size(max = 45)
    @Column(name = "Serial")
    private String serial;
    @Size(max = 45)
    @Column(name = "Imagen")
    private String imagen;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProducto")
    private Collection<Inventarios> inventariosCollection;
    @JoinColumn(name = "Id_Categoria", referencedColumnName = "Id_Tipo_Producto")
    @ManyToOne(optional = false)
    private TipoProducto idCategoria;
    @JoinColumn(name = "Id_Impuesto", referencedColumnName = "Id_Impuesto")
    @ManyToOne(optional = false)
    private Impuestos idImpuesto;
    @JoinColumn(name = "Id_Marca", referencedColumnName = "Id_Marca")
    @ManyToOne(optional = false)
    private Marcas idMarca;

    public Productos() {
    }

    public Productos(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Productos(Integer idProducto, String modeloReferencia, String descripcion, double costo, double precio) {
        this.idProducto = idProducto;
        this.modeloReferencia = modeloReferencia;
        this.descripcion = descripcion;
        this.costo = costo;
        this.precio = precio;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getModeloReferencia() {
        return modeloReferencia;
    }

    public void setModeloReferencia(String modeloReferencia) {
        this.modeloReferencia = modeloReferencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @XmlTransient
    public Collection<Inventarios> getInventariosCollection() {
        return inventariosCollection;
    }

    public void setInventariosCollection(Collection<Inventarios> inventariosCollection) {
        this.inventariosCollection = inventariosCollection;
    }

    public TipoProducto getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(TipoProducto idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Impuestos getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(Impuestos idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public Marcas getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(Marcas idMarca) {
        this.idMarca = idMarca;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Productos)) {
            return false;
        }
        Productos other = (Productos) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.Productos[ idProducto=" + idProducto + " ]";
    }
    
}
