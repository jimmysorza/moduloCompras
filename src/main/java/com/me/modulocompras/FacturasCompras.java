/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.me.modulocompras;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jimmysorza
 */
@Entity
@Table(name = "facturas_compras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FacturasCompras.findAll", query = "SELECT f FROM FacturasCompras f")
    , @NamedQuery(name = "FacturasCompras.findByIdFacturaCompra", query = "SELECT f FROM FacturasCompras f WHERE f.idFacturaCompra = :idFacturaCompra")
    , @NamedQuery(name = "FacturasCompras.findByFecha", query = "SELECT f FROM FacturasCompras f WHERE f.fecha = :fecha")
    , @NamedQuery(name = "FacturasCompras.findByNumeroFactura", query = "SELECT f FROM FacturasCompras f WHERE f.numeroFactura = :numeroFactura")
    , @NamedQuery(name = "FacturasCompras.findBySubTotal", query = "SELECT f FROM FacturasCompras f WHERE f.subTotal = :subTotal")
    , @NamedQuery(name = "FacturasCompras.findByDescuentoTotal", query = "SELECT f FROM FacturasCompras f WHERE f.descuentoTotal = :descuentoTotal")
    , @NamedQuery(name = "FacturasCompras.findByImpuesto", query = "SELECT f FROM FacturasCompras f WHERE f.impuesto = :impuesto")
    , @NamedQuery(name = "FacturasCompras.findByTotal", query = "SELECT f FROM FacturasCompras f WHERE f.total = :total")})
public class FacturasCompras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id_Factura_Compra")
    private Integer idFacturaCompra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "numeroFactura")
    private String numeroFactura;
    @Size(max = 45)
    @Column(name = "SubTotal")
    private String subTotal;
    @Column(name = "Descuento_Total")
    @Temporal(TemporalType.TIMESTAMP)
    private Date descuentoTotal;
    @Size(max = 45)
    @Column(name = "Impuesto")
    private String impuesto;
    @Size(max = 45)
    @Column(name = "Total")
    private String total;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFacturaCompra")
    private Collection<PagosCompras> pagosComprasCollection;
    @JoinColumn(name = "Id_Formas_Pago", referencedColumnName = "Id_Formas_Pago")
    @ManyToOne(optional = false)
    private FormasPago idFormasPago;
    @JoinColumn(name = "Id_Proveedor", referencedColumnName = "Id_Persona")
    @ManyToOne(optional = false)
    private Personas idProveedor;
    @JoinColumn(name = "Id_Sucursal", referencedColumnName = "Id_Sucursal")
    @ManyToOne(optional = false)
    private Sucursales idSucursal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFacturaCompra")
    private Collection<DetalleFacturaCompras> detalleFacturaComprasCollection;

    public FacturasCompras() {
    }

    public FacturasCompras(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public FacturasCompras(Integer idFacturaCompra, Date fecha, String numeroFactura) {
        this.idFacturaCompra = idFacturaCompra;
        this.fecha = fecha;
        this.numeroFactura = numeroFactura;
    }

    public Integer getIdFacturaCompra() {
        return idFacturaCompra;
    }

    public void setIdFacturaCompra(Integer idFacturaCompra) {
        this.idFacturaCompra = idFacturaCompra;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(String numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public Date getDescuentoTotal() {
        return descuentoTotal;
    }

    public void setDescuentoTotal(Date descuentoTotal) {
        this.descuentoTotal = descuentoTotal;
    }

    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @XmlTransient
    public Collection<PagosCompras> getPagosComprasCollection() {
        return pagosComprasCollection;
    }

    public void setPagosComprasCollection(Collection<PagosCompras> pagosComprasCollection) {
        this.pagosComprasCollection = pagosComprasCollection;
    }

    public FormasPago getIdFormasPago() {
        return idFormasPago;
    }

    public void setIdFormasPago(FormasPago idFormasPago) {
        this.idFormasPago = idFormasPago;
    }

    public Personas getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Personas idProveedor) {
        this.idProveedor = idProveedor;
    }

    public Sucursales getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Sucursales idSucursal) {
        this.idSucursal = idSucursal;
    }

    @XmlTransient
    public Collection<DetalleFacturaCompras> getDetalleFacturaComprasCollection() {
        return detalleFacturaComprasCollection;
    }

    public void setDetalleFacturaComprasCollection(Collection<DetalleFacturaCompras> detalleFacturaComprasCollection) {
        this.detalleFacturaComprasCollection = detalleFacturaComprasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFacturaCompra != null ? idFacturaCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FacturasCompras)) {
            return false;
        }
        FacturasCompras other = (FacturasCompras) object;
        if ((this.idFacturaCompra == null && other.idFacturaCompra != null) || (this.idFacturaCompra != null && !this.idFacturaCompra.equals(other.idFacturaCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.me.modulocompras.FacturasCompras[ idFacturaCompra=" + idFacturaCompra + " ]";
    }
    
}
